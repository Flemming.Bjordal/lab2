package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge{
    
    //Field variables:
    ArrayList<FridgeItem> items; // Fridge-object consists of a list of the items 'inside' it.

    //Fridge-object constructor:
    public Fridge() {
        items = new ArrayList<FridgeItem>();
	}

    @Override
    public int totalSize() {
        int maxSize = 20;
        return maxSize;        
    }

    @Override
    public int nItemsInFridge() {
        return this.items.size();
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (this.nItemsInFridge() < this.totalSize()) { //We can only place in a new FridgeItem if the fridge is not full.
            this.items.add(item);
            return true;
        }
        return false;
    }

    @Override
    public void takeOut(FridgeItem item) {
        if (!this.items.contains(item)) {
            throw new NoSuchElementException();
        }
        this.items.remove(item);
    }

    @Override
    public void emptyFridge() {
        this.items.clear();
        
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        ArrayList<FridgeItem> expiredItems = new ArrayList<FridgeItem>();
        for (FridgeItem item : this.items) {
            if (item.hasExpired()) {
                expiredItems.add(item);
            }
        }

        this.items.removeAll(expiredItems);

        return expiredItems;
    }
}
